package com.ruoyi.web.controller.leaseManger;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.bookManger.domain.BookBooks;
import com.ruoyi.bookManger.domain.BookPublishers;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.core.domain.entity.SysUser;
import io.swagger.annotations.ApiModel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 借出信息对象 book_lend_books
 * 
 * @author CuiGuoHao
 * @date 2023-07-29
 */
@ApiModel(value = "BookLendBooks", description = "借书实体")
public class BookLendBooksEntity extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 借书ID */
    private Long lendBookId;

    /** 图书ID */
    @Excel(name = "图书ID")
    private String bookId;

    /** 借书数量 */
    @Excel(name = "借书数量")
    private String lendBookNum;

    /** 借书人ID */
    @Excel(name = "借书人ID")
    private String userId;

    /** 借书时间状态 */
    @Excel(name = "借书时间状态")
    private String lendTimeStatus;

    /** 借书名称 */
    private BookBooks bookBooks;

    /**
     * 借书用户
     */
    private SysUser sysUser;


    private BookPublishers bookPublishers;

    public BookPublishers getBookPublishers() {
        return bookPublishers;
    }

    public void setBookPublishers(BookPublishers bookPublishers) {
        this.bookPublishers = bookPublishers;
    }

    public SysUser getSysUser() {
        return sysUser;
    }

    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }

    public BookBooks getBookBooks() {
        return bookBooks;
    }

    public void setBookBooks(BookBooks bookBooks) {
        this.bookBooks = bookBooks;
    }

    /** 归还时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "归还时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date returnTime;

    /** 最晚时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最晚时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    public void setLendBookId(Long lendBookId) 
    {
        this.lendBookId = lendBookId;
    }

    public Long getLendBookId() 
    {
        return lendBookId;
    }
    public void setBookId(String bookId) 
    {
        this.bookId = bookId;
    }

    public String getBookId() 
    {
        return bookId;
    }
    public void setLendBookNum(String lendBookNum) 
    {
        this.lendBookNum = lendBookNum;
    }

    public String getLendBookNum() 
    {
        return lendBookNum;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setLendTimeStatus(String lendTimeStatus) 
    {
        this.lendTimeStatus = lendTimeStatus;
    }

    public String getLendTimeStatus() 
    {
        return lendTimeStatus;
    }
    public void setReturnTime(Date returnTime) 
    {
        this.returnTime = returnTime;
    }

    public Date getReturnTime() 
    {
        return returnTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("lendBookId", getLendBookId())
            .append("bookId", getBookId())
            .append("lendBookNum", getLendBookNum())
            .append("userId", getUserId())
            .append("createBy", getCreateBy())
            .append("lendTimeStatus", getLendTimeStatus())
            .append("returnTime", getReturnTime())
            .append("endTime", getEndTime())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("BookBooks",getBookBooks())
            .append("sysUser",getSysUser())
            .append("bookPublishers",getBookPublishers())
            .toString();
    }
}
