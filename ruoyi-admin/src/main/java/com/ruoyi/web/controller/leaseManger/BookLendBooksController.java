package com.ruoyi.web.controller.leaseManger;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.leaseManger.domain.BookReturnBooks;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.leaseManger.domain.BookLendBooks;
import com.ruoyi.leaseManger.service.IBookLendBooksService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 借出信息Controller
 * 
 * @author CuiGuoHao
 * @date 2023-07-29
 */
@Api("借书信息管理")
@RestController
@RequestMapping("/leaseManger/bookLend")
public class BookLendBooksController extends BaseController
{
    @Autowired
    private IBookLendBooksService bookLendBooksService;

    /**
     * 查询借出信息列表
     */
    @ApiOperation("查询借出信息列表")
    @PreAuthorize("@ss.hasPermi('leaseManger:bookLend:list')")
    @GetMapping("/list")
    public TableDataInfo list(BookLendBooks bookLendBooks)
    {
        startPage();
        //根据权限请求数据
        SysUser user = SecurityUtils.getLoginUser().getUser();
        List<SysRole> roles = user.getRoles();

        boolean flagIsCommonUser = true;
        for (SysRole role : roles) {
            if (role.getRoleId() !=2 ){
                flagIsCommonUser = false;
            }
        }
        if (flagIsCommonUser == true){
            bookLendBooks.setUserId(String.valueOf(user.getUserId()));
            List<BookLendBooks> list = bookLendBooksService.selectBookLendBooksList(bookLendBooks);
            return getDataTable(list);
        }
        List<BookLendBooks> list = bookLendBooksService.selectBookLendBooksList(bookLendBooks);
        return getDataTable(list);
    }

    /**
     * 导出借出信息列表
     */
    @ApiOperation("导出借出信息列表")
    @PreAuthorize("@ss.hasPermi('leaseManger:bookLend:export')")
    @Log(title = "借出信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BookLendBooks bookLendBooks)
    {
        List<BookLendBooks> list = bookLendBooksService.selectBookLendBooksList(bookLendBooks);
        ExcelUtil<BookLendBooks> util = new ExcelUtil<BookLendBooks>(BookLendBooks.class);
        util.exportExcel(response, list, "借出信息数据");
    }

    /**
     * 获取借出信息详细信息
     */
    @ApiOperation("获取借出信息详细信息")
    @PreAuthorize("@ss.hasPermi('leaseManger:bookLend:query')")
    @GetMapping(value = "/{lendBookId}")
    public AjaxResult getInfo(@PathVariable("lendBookId") Long lendBookId)
    {
        return success(bookLendBooksService.selectBookLendBooksByLendBookId(lendBookId));
    }

    /**
     * 新增借出信息
     */
    @ApiOperation("新增借出信息")
    @PreAuthorize("@ss.hasPermi('leaseManger:bookLend:add')")
    @Log(title = "借出信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BookLendBooks bookLendBooks)
    {
        int i = bookLendBooksService.insertBookLendBooks(bookLendBooks);
        if(i == 0){
            return error("借书失败,借书超过十本/逾期不还书/非图书馆用户");
        }
        return toAjax(i);
    }

    /**
     * 修改借出信息
     */
    @ApiOperation("修改借出信息")
    @PreAuthorize("@ss.hasPermi('leaseManger:bookLend:edit')")
    @Log(title = "借出信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BookLendBooks bookLendBooks)
    {
        return toAjax(bookLendBooksService.updateBookLendBooks(bookLendBooks));
    }

    /**
     * 删除借出信息
     */
    @ApiOperation("删除借出信息")
    @PreAuthorize("@ss.hasPermi('leaseManger:bookLend:remove')")
    @Log(title = "借出信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{lendBookIds}")
    public AjaxResult remove(@PathVariable Long[] lendBookIds)
    {
        return toAjax(bookLendBooksService.deleteBookLendBooksByLendBookIds(lendBookIds));
    }
}
