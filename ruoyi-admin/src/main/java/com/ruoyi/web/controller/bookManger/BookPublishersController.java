package com.ruoyi.web.controller.bookManger;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.bookManger.domain.BookPublishers;
import com.ruoyi.bookManger.service.IBookPublishersService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 出版商信息Controller
 * 
 * @author CuiGuoHao
 * @date 2023-07-29
 */
@Api("出版商信息管理")
@RestController
@RequestMapping("/bookManger/publishers")
public class BookPublishersController extends BaseController
{
    @Autowired
    private IBookPublishersService bookPublishersService;

    /**
     * 查询出版商信息列表
     */
    @ApiOperation("查询出版商信息列表")
    @PreAuthorize("@ss.hasPermi('bookManger:publishers:list')")
    @GetMapping("/list")
    public TableDataInfo list(BookPublishers bookPublishers)
    {
        startPage();
        List<BookPublishers> list = bookPublishersService.selectBookPublishersList(bookPublishers);
        return getDataTable(list);
    }

    /**
     * 导出出版商信息列表
     */
    @ApiOperation("导出出版商信息列表")
    @PreAuthorize("@ss.hasPermi('bookManger:publishers:export')")
    @Log(title = "出版商信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BookPublishers bookPublishers)
    {
        List<BookPublishers> list = bookPublishersService.selectBookPublishersList(bookPublishers);
        ExcelUtil<BookPublishers> util = new ExcelUtil<BookPublishers>(BookPublishers.class);
        util.exportExcel(response, list, "出版商信息数据");
    }

    /**
     * 获取出版商信息详细信息
     */
    @ApiOperation("获取出版商信息详细信息")
    @PreAuthorize("@ss.hasPermi('bookManger:publishers:query')")
    @GetMapping(value = "/{publisherId}")
    public AjaxResult getInfo(@PathVariable("publisherId") Long publisherId)
    {
        return success(bookPublishersService.selectBookPublishersByPublisherId(publisherId));
    }

    /**
     * 新增出版商信息
     */
    @ApiOperation("新增出版商信息")
    @PreAuthorize("@ss.hasPermi('bookManger:publishers:add')")
    @Log(title = "出版商信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BookPublishers bookPublishers)
    {
        return toAjax(bookPublishersService.insertBookPublishers(bookPublishers));
    }

    /**
     * 修改出版商信息
     */
    @ApiOperation("修改出版商信息")
    @PreAuthorize("@ss.hasPermi('bookManger:publishers:edit')")
    @Log(title = "出版商信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BookPublishers bookPublishers)
    {
        return toAjax(bookPublishersService.updateBookPublishers(bookPublishers));
    }

    /**
     * 删除出版商信息
     */
    @ApiOperation("删除出版商信息")
    @PreAuthorize("@ss.hasPermi('bookManger:publishers:remove')")
    @Log(title = "出版商信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{publisherIds}")
    public AjaxResult remove(@PathVariable Long[] publisherIds)
    {
        return toAjax(bookPublishersService.deleteBookPublishersByPublisherIds(publisherIds));
    }
}
