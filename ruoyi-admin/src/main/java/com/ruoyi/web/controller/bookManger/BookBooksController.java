package com.ruoyi.web.controller.bookManger;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.bookManger.domain.BookBooks;
import com.ruoyi.bookManger.service.IBookBooksService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 图书信息Controller
 * 
 * @author CuiGuoHao
 * @date 2023-07-29
 */
@Api("图书信息管理")
@RestController
@RequestMapping("/bookManger/books")
public class BookBooksController extends BaseController
{
    @Autowired
    private IBookBooksService bookBooksService;

    /**
     * 查询图书信息列表
     */
    @ApiOperation("查询图书信息列表")
    @PreAuthorize("@ss.hasPermi('bookManger:books:list')")
    @GetMapping("/list")
    public TableDataInfo list(BookBooks bookBooks)
    {
        startPage();
        List<BookBooks> list = bookBooksService.selectBookBooksList(bookBooks);
        return getDataTable(list);
    }

    /**
     * 导出图书信息列表
     */
    @ApiOperation("导出图书信息列表")
    @PreAuthorize("@ss.hasPermi('bookManger:books:export')")
    @Log(title = "图书信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BookBooks bookBooks)
    {
        List<BookBooks> list = bookBooksService.selectBookBooksList(bookBooks);
        ExcelUtil<BookBooks> util = new ExcelUtil<BookBooks>(BookBooks.class);
        util.exportExcel(response, list, "图书信息数据");
    }

    /**
     * 获取图书信息详细信息
     */
    @ApiOperation("获取图书信息详细信息")
    @PreAuthorize("@ss.hasPermi('bookManger:books:query')")
    @GetMapping(value = "/{bookId}")
    public AjaxResult getInfo(@PathVariable("bookId") Long bookId)
    {
        return success(bookBooksService.selectBookBooksByBookId(bookId));
    }

    /**
     * 新增图书信息
     */
    @ApiOperation("新增图书信息")
    @PreAuthorize("@ss.hasPermi('bookManger:books:add')")
    @Log(title = "图书信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BookBooks bookBooks)
    {
        return toAjax(bookBooksService.insertBookBooks(bookBooks));
    }

    /**
     * 修改图书信息
     */
    @ApiOperation("修改图书信息")
    @PreAuthorize("@ss.hasPermi('bookManger:books:edit')")
    @Log(title = "图书信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BookBooks bookBooks)
    {
        return toAjax(bookBooksService.updateBookBooks(bookBooks));
    }

    /**
     * 删除图书信息
     */
    @ApiOperation("删除图书信息")
    @PreAuthorize("@ss.hasPermi('bookManger:books:remove')")
    @Log(title = "图书信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{bookIds}")
    public AjaxResult remove(@PathVariable Long[] bookIds)
    {
        return toAjax(bookBooksService.deleteBookBooksByBookIds(bookIds));
    }
}
