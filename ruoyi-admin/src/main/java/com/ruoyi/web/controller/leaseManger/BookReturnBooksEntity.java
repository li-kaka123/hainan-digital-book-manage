package com.ruoyi.web.controller.leaseManger;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.bookManger.domain.BookBooks;
import com.ruoyi.bookManger.domain.BookPublishers;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.core.domain.entity.SysUser;
import io.swagger.annotations.ApiModel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 归还信息对象 book_return_books
 * 
 * @author CuiGuoHao
 * @date 2023-07-29
 */
@ApiModel(value = "BookReturnBooks", description = "还书实体")
public class BookReturnBooksEntity extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 借书ID */
    private Long returnBookId;

    /** 图书ID */
    private String bookId;

    /** 借书数量 */
    private String returnBookNum;

    /** 借书人ID */
    @Excel(name = "借书人ID")
    private String userId;

    /** 借书时间状态 */
    @Excel(name = "借书时间状态")
    private String returnTimeStatus;

    /** 归还时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "归还时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date returnTime;

    /** 最晚时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最晚时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 借书名称 */
    private BookBooks bookBooks;

    /**
     * 借书用户
     */
    private SysUser sysUser;

    private BookPublishers bookPublishers;

    public BookPublishers getBookPublishers() {
        return bookPublishers;
    }

    public void setBookPublishers(BookPublishers bookPublishers) {
        this.bookPublishers = bookPublishers;
    }


    public SysUser getSysUser() {
        return sysUser;
    }

    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }

    public BookBooks getBookBooks() {
        return bookBooks;
    }

    public void setBookBooks(BookBooks bookBooks) {
        this.bookBooks = bookBooks;
    }

    public void setReturnBookId(Long returnBookId) 
    {
        this.returnBookId = returnBookId;
    }

    public Long getReturnBookId() 
    {
        return returnBookId;
    }
    public void setBookId(String bookId) 
    {
        this.bookId = bookId;
    }

    public String getBookId() 
    {
        return bookId;
    }
    public void setReturnBookNum(String returnBookNum) 
    {
        this.returnBookNum = returnBookNum;
    }

    public String getReturnBookNum() 
    {
        return returnBookNum;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setReturnTimeStatus(String returnTimeStatus) 
    {
        this.returnTimeStatus = returnTimeStatus;
    }

    public String getReturnTimeStatus() 
    {
        return returnTimeStatus;
    }
    public void setReturnTime(Date returnTime) 
    {
        this.returnTime = returnTime;
    }

    public Date getReturnTime() 
    {
        return returnTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("returnBookId", getReturnBookId())
            .append("bookId", getBookId())
            .append("returnBookNum", getReturnBookNum())
            .append("userId", getUserId())
            .append("createBy", getCreateBy())
            .append("returnTimeStatus", getReturnTimeStatus())
            .append("returnTime", getReturnTime())
            .append("endTime", getEndTime())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("BookBooks",getBookBooks())
            .append("sysUser",getSysUser())
            .append("bookPublishers",getBookPublishers())
            .toString();
    }
}
