package com.ruoyi.web.controller.leaseManger;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.leaseManger.domain.BookReturnBooks;
import com.ruoyi.leaseManger.service.IBookReturnBooksService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 归还信息Controller
 * 
 * @author CuiGuoHao
 * @date 2023-07-29
 */
@Api("还书信息管理")
@RestController
@RequestMapping("/leaseManger/bookReturn")
public class BookReturnBooksController extends BaseController
{
    @Autowired
    private IBookReturnBooksService bookReturnBooksService;

    /**
     * 查询归还信息列表
     */
    @ApiOperation("查询归还信息列表")
    @PreAuthorize("@ss.hasPermi('leaseManger:bookReturn:list')")
    @GetMapping("/list")
    public TableDataInfo list(BookReturnBooks bookReturnBooks)
    {
        startPage();
        //根据权限请求数据
        SysUser user = SecurityUtils.getLoginUser().getUser();
        List<SysRole> roles = user.getRoles();

        boolean flagIsCommonUser = true;
        for (SysRole role : roles) {
            if (role.getRoleId() !=2 ){
                flagIsCommonUser = false;
            }
        }
        if (flagIsCommonUser == true){
            bookReturnBooks.setUserId(String.valueOf(user.getUserId()));
            List<BookReturnBooks> list = bookReturnBooksService.selectBookReturnBooksList(bookReturnBooks);
            return getDataTable(list);
        }
        List<BookReturnBooks> list = bookReturnBooksService.selectBookReturnBooksList(bookReturnBooks);
        return getDataTable(list);

    }


    /**
     * 导出归还信息列表
     */
    @ApiOperation("导出归还信息列表")
    @PreAuthorize("@ss.hasPermi('leaseManger:bookReturn:export')")
    @Log(title = "归还信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BookReturnBooks bookReturnBooks)
    {
        List<BookReturnBooks> list = bookReturnBooksService.selectBookReturnBooksList(bookReturnBooks);
        ExcelUtil<BookReturnBooks> util = new ExcelUtil<BookReturnBooks>(BookReturnBooks.class);
        util.exportExcel(response, list, "归还信息数据");
    }

    /**
     * 获取归还信息详细信息
     */
    @ApiOperation("获取归还信息详细信息")
    @PreAuthorize("@ss.hasPermi('leaseManger:bookReturn:query')")
    @GetMapping(value = "/{returnBookId}")
    public AjaxResult getInfo(@PathVariable("returnBookId") Long returnBookId)
    {
        return success(bookReturnBooksService.selectBookReturnBooksByReturnBookId(returnBookId));
    }

    /**
     * 新增归还信息
     */
    @ApiOperation("新增归还信息")
    @PreAuthorize("@ss.hasPermi('leaseManger:bookReturn:add')")
    @Log(title = "归还信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BookReturnBooks bookReturnBooks)
    {

        int i = bookReturnBooksService.insertBookReturnBooks(bookReturnBooks);
        if(i == 0){
            return error("还书失败,用户非法/该书不属于图书馆/还书数量对不上");
        }
        return toAjax(i);
    }

    /**
     * 修改归还信息
     */
    @ApiOperation("修改归还信息")
    @PreAuthorize("@ss.hasPermi('leaseManger:bookReturn:edit')")
    @Log(title = "归还信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BookReturnBooks bookReturnBooks)
    {
        return toAjax(bookReturnBooksService.updateBookReturnBooks(bookReturnBooks));
    }

    /**
     * 删除归还信息
     */
    @ApiOperation("删除归还信息")
    @PreAuthorize("@ss.hasPermi('leaseManger:bookReturn:remove')")
    @Log(title = "归还信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{returnBookIds}")
    public AjaxResult remove(@PathVariable Long[] returnBookIds)
    {
        return toAjax(bookReturnBooksService.deleteBookReturnBooksByReturnBookIds(returnBookIds));
    }
}
