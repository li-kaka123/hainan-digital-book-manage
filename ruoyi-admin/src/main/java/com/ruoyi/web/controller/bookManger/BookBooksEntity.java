package com.ruoyi.web.controller.bookManger;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.bookManger.domain.BookPublishers;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 图书信息对象 book_books
 * 
 * @author CuiGuoHao
 * @date 2023-07-29
 */
@ApiModel(value = "BookBooks", description = "图书实体")
public class BookBooksEntity extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 图书ID */
    private Long bookId;

    /** 图书名称 */
    @Excel(name = "图书名称")
    private String bookSssname;

    /** 图书编号 */
    @Excel(name = "图书编号")
    private String bookSsscode;

    /** 图书作者 */
    @Excel(name = "图书作者")
    private String bookWriter;

    /** 出版社ID */
    @Excel(name = "出版社ID")
    private String publisherId;

    /** 图书分类 */
    @Excel(name = "图书分类")
    private String bookVarieties;

    /** 图书品类 */
    @Excel(name = "图书品类")
    private String bookType;

    /** 图书位置 */
    @Excel(name = "图书位置")
    private String bookLocation;

    /** 图书价格 */
    @Excel(name = "图书价格")
    private String bookPrice;

    /** 图书库存量 */
    @Excel(name = "图书库存量")
    private String bookNum;

    /** 图书预警数量 */
    @Excel(name = "图书预警数量")
    private String bookWarnValue;

    /** 图书状态 */
    @Excel(name = "图书状态")
    private String bookStatus;

    /** 出版厂商 */
    private BookPublishers bookPublishers;

    public BookPublishers getBookPublishers() {
        return bookPublishers;
    }

    public void setBookPublishers(BookPublishers bookPublishers) {
        this.bookPublishers = bookPublishers;
    }

    /** 图书出版日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "图书出版日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date bookPublisherTime;

    public void setBookId(Long bookId) 
    {
        this.bookId = bookId;
    }

    public Long getBookId() 
    {
        return bookId;
    }
    public void setBookSssname(String bookSssname) 
    {
        this.bookSssname = bookSssname;
    }

    public String getBookSssname() 
    {
        return bookSssname;
    }
    public void setBookSsscode(String bookSsscode) 
    {
        this.bookSsscode = bookSsscode;
    }

    public String getBookSsscode() 
    {
        return bookSsscode;
    }
    public void setBookWriter(String bookWriter) 
    {
        this.bookWriter = bookWriter;
    }

    public String getBookWriter() 
    {
        return bookWriter;
    }
    public void setPublisherId(String publisherId) 
    {
        this.publisherId = publisherId;
    }

    public String getPublisherId() 
    {
        return publisherId;
    }
    public void setBookVarieties(String bookVarieties) 
    {
        this.bookVarieties = bookVarieties;
    }

    public String getBookVarieties() 
    {
        return bookVarieties;
    }
    public void setBookType(String bookType) 
    {
        this.bookType = bookType;
    }

    public String getBookType() 
    {
        return bookType;
    }
    public void setBookLocation(String bookLocation) 
    {
        this.bookLocation = bookLocation;
    }

    public String getBookLocation() 
    {
        return bookLocation;
    }
    public void setBookPrice(String bookPrice) 
    {
        this.bookPrice = bookPrice;
    }

    public String getBookPrice() 
    {
        return bookPrice;
    }
    public void setBookNum(String bookNum) 
    {
        this.bookNum = bookNum;
    }

    public String getBookNum() 
    {
        return bookNum;
    }
    public void setBookWarnValue(String bookWarnValue) 
    {
        this.bookWarnValue = bookWarnValue;
    }

    public String getBookWarnValue() 
    {
        return bookWarnValue;
    }
    public void setBookStatus(String bookStatus) 
    {
        this.bookStatus = bookStatus;
    }

    public String getBookStatus() 
    {
        return bookStatus;
    }
    public void setBookPublisherTime(Date bookPublisherTime) 
    {
        this.bookPublisherTime = bookPublisherTime;
    }

    public Date getBookPublisherTime() 
    {
        return bookPublisherTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("bookId", getBookId())
            .append("bookSssname", getBookSssname())
            .append("bookSsscode", getBookSsscode())
            .append("bookWriter", getBookWriter())
            .append("publisherId", getPublisherId())
            .append("bookVarieties", getBookVarieties())
            .append("bookType", getBookType())
            .append("bookLocation", getBookLocation())
            .append("bookPrice", getBookPrice())
            .append("bookNum", getBookNum())
            .append("bookWarnValue", getBookWarnValue())
            .append("bookStatus", getBookStatus())
            .append("bookPublisherTime", getBookPublisherTime())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("bookPublishers", getBookPublishers())
            .toString();
    }
}
