# ***\*图书管理系统\****

# ***\*1 需求分析\****

![img](./assets/wps26.jpg) 

# ***\*2 实体关系图\****

指标：

借书逻辑

1 每个人借书已经借书不超过十本，不存在逾期不还的情况

2 图书馆是否有书，有书的话是否大于要借书的数量，如果够的话就扣除图书馆的书

还书逻辑

1 根据userId和bookId先检查是否逾期，如果存在逾期的情况，插入状态显示逾期

2 如果不存在该书籍，则图书馆不进行还

22类目

A 马克思主义、列宁主义、毛泽东思想、邓小平理论
B 哲学、宗教
C 社会科学总论
D 政治、法律
E 军事
F 经济
G 文化、科学、教育、体育
H 语言、文字
I 文学
J 艺术
K 历史、地理
N 自然科学总论
O 数理科学和化学
P 天文学、地球科学
Q 生物科学
R 医药、卫生
S 农业科学
T 工业技术
U 交通运输
V 航空、航天
X 环境科学、劳动保护科学（安全科学）
Z 综合性图书

 

publisher_id
publisher_name
publisher_code
publisher_contact(联系人)
publisher_phone(联系电话)
keyword(关键字)
status(状态)

crate_by
crate_time
update_by
update_time
remark(备注)

book_id
book_name(图书名称)
book_code(图书编号)
publisher_id(出版社)
book_varieties(图书分类)
book_type(图书品类,图书，光盘，杂志，报纸)
book_location(图书位置)
book_price(图书价格)
book_num(图书库存量)
book_warn_value(图书预警数量)
book_status(图书状态)

crate_by
crate_time
update_by
update_time
remark(备注)

 

borrow_book_id(借书ID)
book_id(书籍ID)
borrow_book_num(借书数量)
user_id(借书人ID)
return_time(归还时间)
borrow_time_status(借书时间状态)
end_time(时间内归还)

crate_by
crate_time(借书时间)
update_by
update_time
remark(备注)

 

borrow_book_id(借书ID)
book_id(书籍ID)
borrow_book_num(借书数量)
user_id(借书人ID)
return_time(归还时间)
borrow_time_status(借书时间状态)
end_time(时间内归还)

crate_by
crate_time(借书时间)
update_by
update_time
remark(备注)

# ***\*3 技术实现\****

## ***\*3.1 登录\****

使用JWT技术进行登录

## ***\*3.2 权限验证\****

使用springsecurity进行权限校验，根据角色进行校验

![img](./assets/wps27.jpg) 

## ***\*3.3 分层\****

![img](./assets/wps28.jpg) 

![img](./assets/wps29.jpg) 

## ***\*3.4 日志\****

![img](./assets/wps30.jpg) 

## ***\*3.5 缓存\****

redis用于缓存令牌和验证码，主要操作登录信息

## ***\*3.6 数据库\****

![img](./assets/wps31.jpg) 

## ***\*3.7 配置分离\****

使用docker进行部署

![img](./assets/wps32.jpg) 

## ***\*3.8 swagger\****

地址 http://localhost:8080/swagger-ui/index.html#/

开放swagger和静态资源权限

![img](./assets/wps33.jpg) 

## ***\*3.9 定时任务\****

用于更新图书馆图书状态，借书状态和还书状态，每天跑一次

![img](./assets/wps34.jpg) 

![img](./assets/wps35.jpg) 

## ***\*3.10 事务\****

借书和还书操作是事务操作

![img](./assets/wps36.jpg) 

## ***\*4 成果展示\****

登录界面

![img](./assets/wps37.jpg) 

首页

![img](./assets/wps38.jpg) 

出版商信息

![img](./assets/wps39.jpg) 

出版商新增

![img](./assets/wps40.jpg) 

图书信息

![img](./assets/wps41.jpg) 

借出管理

![img](./assets/wps42.jpg) 

归还信息

![img](./assets/wps43.jpg) 

借出信息

![img](./assets/wps44-1690907768369-1-1690907790092-4.jpg) 

归还信息

![img](./assets/wps45.jpg) 

# ***\*5 测试账号\****

超级管理员(admin)

admin 123456

整理管理员(tidyAdmin)

tidyTest 123456

租借管理员(leaseAdmin)

zuj 123456

普通用户(commonUser)

wangwu 123456

likaka 123456

## ***\*5.2 分角色看到的内容\****

超级管理员(admin)

![img](./assets/wps46.jpg) 

整理管理员(tidyAdmin)

![img](./assets/wps47.jpg) 

租借管理员

![img](./assets/wps48.jpg) 

![img](./assets/wps49.jpg) 

个人用户

![img](./assets/wps50.jpg)10.0.
