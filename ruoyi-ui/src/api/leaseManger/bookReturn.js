import request from '@/utils/request'

// 查询归还信息列表
export function listBookReturn(query) {
  return request({
    url: '/leaseManger/bookReturn/list',
    method: 'get',
    params: query
  })
}

// 查询归还信息详细
export function getBookReturn(returnBookId) {
  return request({
    url: '/leaseManger/bookReturn/' + returnBookId,
    method: 'get'
  })
}

// 新增归还信息
export function addBookReturn(data) {
  return request({
    url: '/leaseManger/bookReturn',
    method: 'post',
    data: data
  })
}

// 修改归还信息
export function updateBookReturn(data) {
  return request({
    url: '/leaseManger/bookReturn',
    method: 'put',
    data: data
  })
}

// 删除归还信息
export function delBookReturn(returnBookId) {
  return request({
    url: '/leaseManger/bookReturn/' + returnBookId,
    method: 'delete'
  })
}
