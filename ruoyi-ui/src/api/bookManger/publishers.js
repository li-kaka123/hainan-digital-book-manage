import request from '@/utils/request'

// 查询出版商信息列表
export function listPublishers(query) {
  return request({
    url: '/bookManger/publishers/list',
    method: 'get',
    params: query
  })
}

// 查询出版商信息详细
export function getPublishers(publisherId) {
  return request({
    url: '/bookManger/publishers/' + publisherId,
    method: 'get'
  })
}

// 新增出版商信息
export function addPublishers(data) {
  return request({
    url: '/bookManger/publishers',
    method: 'post',
    data: data
  })
}

// 修改出版商信息
export function updatePublishers(data) {
  return request({
    url: '/bookManger/publishers',
    method: 'put',
    data: data
  })
}

// 删除出版商信息
export function delPublishers(publisherId) {
  return request({
    url: '/bookManger/publishers/' + publisherId,
    method: 'delete'
  })
}
