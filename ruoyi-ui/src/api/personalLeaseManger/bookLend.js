import request from '@/utils/request'

// 查询借出信息列表
export function listBookLend(query) {
  return request({
    url: '/leaseManger/bookLend/list',
    method: 'get',
    params: query
  })
}

// 查询借出信息详细
export function getBookLend(lendBookId) {
  return request({
    url: '/leaseManger/bookLend/' + lendBookId,
    method: 'get'
  })
}

// 新增借出信息
export function addBookLend(data) {
  return request({
    url: '/leaseManger/bookLend',
    method: 'post',
    data: data
  })
}

// 修改借出信息
export function updateBookLend(data) {
  return request({
    url: '/leaseManger/bookLend',
    method: 'put',
    data: data
  })
}

// 删除借出信息
export function delBookLend(lendBookId) {
  return request({
    url: '/leaseManger/bookLend/' + lendBookId,
    method: 'delete'
  })
}
