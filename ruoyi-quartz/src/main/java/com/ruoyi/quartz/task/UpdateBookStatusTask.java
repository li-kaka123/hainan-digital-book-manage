package com.ruoyi.quartz.task;

import com.ruoyi.bookManger.service.IBookBooksService;
import com.ruoyi.leaseManger.service.IBookLendBooksService;
import com.ruoyi.leaseManger.service.IBookReturnBooksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("updateBookStatusTask")
public class UpdateBookStatusTask {

    @Autowired
    private IBookBooksService bookBooksService;

    @Autowired
    private IBookLendBooksService bookLendBooksService;

    @Autowired
    private IBookReturnBooksService bookReturnBooksService;

    public void updateBookBooksStatus()
    {
        System.out.println("执行图书馆所有书籍更新状态：");
        bookBooksService.updateStatusByEndTime();
    }


    public void updateBookLendStatus()
    {
        System.out.println("执行借阅书籍更新状态：");
        //使用MyBatis更新
        bookLendBooksService.updateStatusByEndTime();
    }

    public void updateBookReturnStatus()
    {
        System.out.println("执行还回书籍更新状态：");
        bookReturnBooksService.updateStatusByEndTime();
    }

}
