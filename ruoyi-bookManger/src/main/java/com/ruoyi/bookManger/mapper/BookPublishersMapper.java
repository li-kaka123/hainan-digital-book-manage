package com.ruoyi.bookManger.mapper;

import java.util.List;
import com.ruoyi.bookManger.domain.BookPublishers;

/**
 * 出版商信息Mapper接口
 * 
 * @author CuiGuoHao
 * @date 2023-07-29
 */
public interface BookPublishersMapper 
{
    /**
     * 查询出版商信息
     * 
     * @param publisherId 出版商信息主键
     * @return 出版商信息
     */
    public BookPublishers selectBookPublishersByPublisherId(Long publisherId);

    /**
     * 查询出版商信息列表
     * 
     * @param bookPublishers 出版商信息
     * @return 出版商信息集合
     */
    public List<BookPublishers> selectBookPublishersList(BookPublishers bookPublishers);

    /**
     * 新增出版商信息
     * 
     * @param bookPublishers 出版商信息
     * @return 结果
     */
    public int insertBookPublishers(BookPublishers bookPublishers);

    /**
     * 修改出版商信息
     * 
     * @param bookPublishers 出版商信息
     * @return 结果
     */
    public int updateBookPublishers(BookPublishers bookPublishers);

    /**
     * 删除出版商信息
     * 
     * @param publisherId 出版商信息主键
     * @return 结果
     */
    public int deleteBookPublishersByPublisherId(Long publisherId);

    /**
     * 批量删除出版商信息
     * 
     * @param publisherIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBookPublishersByPublisherIds(Long[] publisherIds);
}
