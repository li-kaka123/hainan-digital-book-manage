package com.ruoyi.bookManger.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.bookManger.mapper.BookBooksMapper;
import com.ruoyi.bookManger.domain.BookBooks;
import com.ruoyi.bookManger.service.IBookBooksService;

/**
 * 图书信息Service业务层处理
 * 
 * @author CuiGuoHao
 * @date 2023-07-29
 */
@Service
public class BookBooksServiceImpl implements IBookBooksService 
{
    @Autowired
    private BookBooksMapper bookBooksMapper;

    /**
     * 查询图书信息
     * 
     * @param bookId 图书信息主键
     * @return 图书信息
     */
    @Override
    public BookBooks selectBookBooksByBookId(Long bookId)
    {
        return bookBooksMapper.selectBookBooksByBookId(bookId);
    }

    /**
     * 查询图书信息列表
     * 
     * @param bookBooks 图书信息
     * @return 图书信息
     */
    @Override
    public List<BookBooks> selectBookBooksList(BookBooks bookBooks)
    {
        return bookBooksMapper.selectBookBooksList(bookBooks);
    }

    /**
     * 新增图书信息
     * 
     * @param bookBooks 图书信息
     * @return 结果
     */
    @Override
    public int insertBookBooks(BookBooks bookBooks)
    {
        bookBooks.setCreateTime(DateUtils.getNowDate());
        return bookBooksMapper.insertBookBooks(bookBooks);
    }

    /**
     * 修改图书信息
     * 
     * @param bookBooks 图书信息
     * @return 结果
     */
    @Override
    public int updateBookBooks(BookBooks bookBooks)
    {
        bookBooks.setUpdateTime(DateUtils.getNowDate());
        return bookBooksMapper.updateBookBooks(bookBooks);
    }

    /**
     * 批量删除图书信息
     * 
     * @param bookIds 需要删除的图书信息主键
     * @return 结果
     */
    @Override
    public int deleteBookBooksByBookIds(Long[] bookIds)
    {
        return bookBooksMapper.deleteBookBooksByBookIds(bookIds);
    }

    /**
     * 删除图书信息信息
     * 
     * @param bookId 图书信息主键
     * @return 结果
     */
    @Override
    public int deleteBookBooksByBookId(Long bookId)
    {
        return bookBooksMapper.deleteBookBooksByBookId(bookId);
    }

    @Override
    public void updateStatusByEndTime() {
        bookBooksMapper.updateStatusByEndTime();
    }
}
