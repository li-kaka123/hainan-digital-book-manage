package com.ruoyi.bookManger.service;

import java.util.List;
import com.ruoyi.bookManger.domain.BookBooks;

/**
 * 图书信息Service接口
 * 
 * @author CuiGuoHao
 * @date 2023-07-29
 */
public interface IBookBooksService 
{
    /**
     * 查询图书信息
     * 
     * @param bookId 图书信息主键
     * @return 图书信息
     */
    public BookBooks selectBookBooksByBookId(Long bookId);

    /**
     * 查询图书信息列表
     * 
     * @param bookBooks 图书信息
     * @return 图书信息集合
     */
    public List<BookBooks> selectBookBooksList(BookBooks bookBooks);

    /**
     * 新增图书信息
     * 
     * @param bookBooks 图书信息
     * @return 结果
     */
    public int insertBookBooks(BookBooks bookBooks);

    /**
     * 修改图书信息
     * 
     * @param bookBooks 图书信息
     * @return 结果
     */
    public int updateBookBooks(BookBooks bookBooks);

    /**
     * 批量删除图书信息
     * 
     * @param bookIds 需要删除的图书信息主键集合
     * @return 结果
     */
    public int deleteBookBooksByBookIds(Long[] bookIds);

    /**
     * 删除图书信息信息
     * 
     * @param bookId 图书信息主键
     * @return 结果
     */
    public int deleteBookBooksByBookId(Long bookId);

    void updateStatusByEndTime();
}
