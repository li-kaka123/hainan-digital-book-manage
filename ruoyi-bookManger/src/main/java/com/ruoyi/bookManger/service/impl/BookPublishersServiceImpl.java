package com.ruoyi.bookManger.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.bookManger.mapper.BookPublishersMapper;
import com.ruoyi.bookManger.domain.BookPublishers;
import com.ruoyi.bookManger.service.IBookPublishersService;

/**
 * 出版商信息Service业务层处理
 * 
 * @author CuiGuoHao
 * @date 2023-07-29
 */
@Service
public class BookPublishersServiceImpl implements IBookPublishersService 
{
    @Autowired
    private BookPublishersMapper bookPublishersMapper;

    /**
     * 查询出版商信息
     * 
     * @param publisherId 出版商信息主键
     * @return 出版商信息
     */
    @Override
    public BookPublishers selectBookPublishersByPublisherId(Long publisherId)
    {
        return bookPublishersMapper.selectBookPublishersByPublisherId(publisherId);
    }

    /**
     * 查询出版商信息列表
     * 
     * @param bookPublishers 出版商信息
     * @return 出版商信息
     */
    @Override
    public List<BookPublishers> selectBookPublishersList(BookPublishers bookPublishers)
    {
        return bookPublishersMapper.selectBookPublishersList(bookPublishers);
    }

    /**
     * 新增出版商信息
     * 
     * @param bookPublishers 出版商信息
     * @return 结果
     */
    @Override
    public int insertBookPublishers(BookPublishers bookPublishers)
    {
        bookPublishers.setCreateTime(DateUtils.getNowDate());
        return bookPublishersMapper.insertBookPublishers(bookPublishers);
    }

    /**
     * 修改出版商信息
     * 
     * @param bookPublishers 出版商信息
     * @return 结果
     */
    @Override
    public int updateBookPublishers(BookPublishers bookPublishers)
    {
        bookPublishers.setUpdateTime(DateUtils.getNowDate());
        return bookPublishersMapper.updateBookPublishers(bookPublishers);
    }

    /**
     * 批量删除出版商信息
     * 
     * @param publisherIds 需要删除的出版商信息主键
     * @return 结果
     */
    @Override
    public int deleteBookPublishersByPublisherIds(Long[] publisherIds)
    {
        return bookPublishersMapper.deleteBookPublishersByPublisherIds(publisherIds);
    }

    /**
     * 删除出版商信息信息
     * 
     * @param publisherId 出版商信息主键
     * @return 结果
     */
    @Override
    public int deleteBookPublishersByPublisherId(Long publisherId)
    {
        return bookPublishersMapper.deleteBookPublishersByPublisherId(publisherId);
    }
}
