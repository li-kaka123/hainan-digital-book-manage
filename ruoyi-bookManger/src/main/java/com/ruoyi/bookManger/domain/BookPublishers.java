package com.ruoyi.bookManger.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 出版商信息对象 book_publishers
 * 
 * @author CuiGuoHao
 * @date 2023-07-29
 */
public class BookPublishers extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 出版社ID */
    @Excel(name = "出版社ID")
    private Long publisherId;

    /** 出版社名称 */
    @Excel(name = "出版社名称")
    private String publisherName;

    /** 出版社编码 */
    @Excel(name = "出版社编码")
    private String publisherCode;

    /** 出版社联系人 */
    @Excel(name = "出版社联系人")
    private String publisherContact;

    /** 出版社电话 */
    @Excel(name = "出版社电话")
    private String publisherPhone;

    /** 出版社关键字 */
    @Excel(name = "出版社关键字")
    private String publisheKeyword;

    /** 状态 */
    @Excel(name = "状态")
    private String publisheStatus;

    /** 出版社等级 */
    @Excel(name = "出版社等级")
    private String publisherRank;

    /** 出版社分类 */
    @Excel(name = "出版社分类")
    private String publisherClassification;

    public void setPublisherId(Long publisherId) 
    {
        this.publisherId = publisherId;
    }

    public Long getPublisherId() 
    {
        return publisherId;
    }
    public void setPublisherName(String publisherName) 
    {
        this.publisherName = publisherName;
    }

    public String getPublisherName() 
    {
        return publisherName;
    }
    public void setPublisherCode(String publisherCode) 
    {
        this.publisherCode = publisherCode;
    }

    public String getPublisherCode() 
    {
        return publisherCode;
    }
    public void setPublisherContact(String publisherContact) 
    {
        this.publisherContact = publisherContact;
    }

    public String getPublisherContact() 
    {
        return publisherContact;
    }
    public void setPublisherPhone(String publisherPhone) 
    {
        this.publisherPhone = publisherPhone;
    }

    public String getPublisherPhone() 
    {
        return publisherPhone;
    }
    public void setPublisheKeyword(String publisheKeyword) 
    {
        this.publisheKeyword = publisheKeyword;
    }

    public String getPublisheKeyword() 
    {
        return publisheKeyword;
    }
    public void setPublisheStatus(String publisheStatus) 
    {
        this.publisheStatus = publisheStatus;
    }

    public String getPublisheStatus() 
    {
        return publisheStatus;
    }
    public void setPublisherRank(String publisherRank) 
    {
        this.publisherRank = publisherRank;
    }

    public String getPublisherRank() 
    {
        return publisherRank;
    }
    public void setPublisherClassification(String publisherClassification) 
    {
        this.publisherClassification = publisherClassification;
    }

    public String getPublisherClassification() 
    {
        return publisherClassification;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("publisherId", getPublisherId())
            .append("publisherName", getPublisherName())
            .append("publisherCode", getPublisherCode())
            .append("publisherContact", getPublisherContact())
            .append("publisherPhone", getPublisherPhone())
            .append("publisheKeyword", getPublisheKeyword())
            .append("publisheStatus", getPublisheStatus())
            .append("publisherRank", getPublisherRank())
            .append("publisherClassification", getPublisherClassification())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
