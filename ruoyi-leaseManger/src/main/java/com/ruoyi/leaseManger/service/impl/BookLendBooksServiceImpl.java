package com.ruoyi.leaseManger.service.impl;

import java.util.List;

import com.ruoyi.bookManger.domain.BookBooks;
import com.ruoyi.bookManger.mapper.BookBooksMapper;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.leaseManger.mapper.BookLendBooksMapper;
import com.ruoyi.leaseManger.domain.BookLendBooks;
import com.ruoyi.leaseManger.service.IBookLendBooksService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 借出信息Service业务层处理
 * 
 * @author CuiGuoHao
 * @date 2023-07-29
 */
@Service
public class BookLendBooksServiceImpl implements IBookLendBooksService 
{
    @Autowired
    private BookLendBooksMapper bookLendBooksMapper;

    @Autowired
    private BookBooksMapper bookBooksMapper;

    @Autowired
    private ISysUserService userService;



    /**
     * 查询借出信息
     * 
     * @param lendBookId 借出信息主键
     * @return 借出信息
     */
    @Override
    public BookLendBooks selectBookLendBooksByLendBookId(Long lendBookId)
    {
        return bookLendBooksMapper.selectBookLendBooksByLendBookId(lendBookId);
    }

    /**
     * 查询借出信息列表
     * 
     * @param bookLendBooks 借出信息
     * @return 借出信息
     */
    @Override
    public List<BookLendBooks> selectBookLendBooksList(BookLendBooks bookLendBooks)
    {
        return bookLendBooksMapper.selectBookLendBooksList(bookLendBooks);
    }

    /**
     * 新增借出信息
     * 
     * @param bookLendBooks 借出信息
     * @return 结果
     */
    @Override
    @Transactional
    public int insertBookLendBooks(BookLendBooks bookLendBooks)
    {
        int lendBookNum = Integer.parseInt(bookLendBooks.getLendBookNum());
        //验证用户是否为本地用户
        SysUser sysUser = userService.selectUserById(Long.parseLong(bookLendBooks.getUserId()));
        if (sysUser == null || (sysUser.getDelFlag().compareTo("0") != 0)){
            return 0;
        }

        //每个人借书不超过十本,不存在逾期不还的情况
        if ( lendBookNum > 10){
            return 0;
        }

        BookLendBooks bookLendBooks2 = new BookLendBooks();
        bookLendBooks2.setUserId(bookLendBooks.getUserId());
        List<BookLendBooks> bookLendBooks1 = bookLendBooksMapper.selectBookLendBooksList(bookLendBooks2);
        int bookCount = 0;
        for (BookLendBooks lendBooks : bookLendBooks1) {

            int status = Integer.parseInt(lendBooks.getLendTimeStatus());
            if (status == 3){
                continue;
            }

            bookCount += Integer.parseInt(lendBooks.getLendBookNum());

            if (bookCount >= 10){
                return 0;
            }

            if (lendBooks.getLendTimeStatus().equals('2')){
                return 0;
            }
        }
        if ((lendBookNum +bookCount) > 10){
            return 0;
        }

        //判断图书馆是否有书,如果有书就检查数量并扣除,如果无书籍,就报错
        BookBooks bookBooks = bookBooksMapper.selectBookBooksByBookId(Long.valueOf(bookLendBooks.getBookId()));
        if (bookBooks == null){
            return 0;
        }
        BookBooks bookBooks1 = new BookBooks();
        int diff = Integer.parseInt(bookBooks.getBookNum()) - lendBookNum;
        if (diff < 0){
                return 0;
        }
        switch (diff){
            case 0 : bookBooks1.setBookStatus("2");break;
            case 1 :
            case 2 : bookBooks1.setBookStatus("1");break;
            default: bookBooks1.setBookStatus("0");break;
        }
        bookBooks1.setBookNum(String.valueOf(diff));
        bookBooks1.setBookId(bookBooks.getBookId());
        bookBooksMapper.updateBookBooks(bookBooks1);


        //添加借书表
        bookLendBooks.setCreateTime(DateUtils.getNowDate());
        bookLendBooks.setLendTimeStatus("0");
        bookLendBooks.setEndTime(DateUtils.addMonths(bookLendBooks.getCreateTime(),2));
        return bookLendBooksMapper.insertBookLendBooks(bookLendBooks);
    }

    /**
     * 修改借出信息
     * 
     * @param bookLendBooks 借出信息
     * @return 结果
     */
    @Override
    public int updateBookLendBooks(BookLendBooks bookLendBooks)
    {
        bookLendBooks.setUpdateTime(DateUtils.getNowDate());
        return bookLendBooksMapper.updateBookLendBooks(bookLendBooks);
    }

    /**
     * 批量删除借出信息
     * 
     * @param lendBookIds 需要删除的借出信息主键
     * @return 结果
     */
    @Override
    public int deleteBookLendBooksByLendBookIds(Long[] lendBookIds)
    {
        return bookLendBooksMapper.deleteBookLendBooksByLendBookIds(lendBookIds);
    }

    /**
     * 删除借出信息信息
     * 
     * @param lendBookId 借出信息主键
     * @return 结果
     */
    @Override
    public int deleteBookLendBooksByLendBookId(Long lendBookId)
    {
        return bookLendBooksMapper.deleteBookLendBooksByLendBookId(lendBookId);
    }

    @Override
    public void updateStatusByEndTime() {
        bookLendBooksMapper.updateStatusByEndTime();
    }
}
