package com.ruoyi.leaseManger.service;

import java.util.List;
import com.ruoyi.leaseManger.domain.BookReturnBooks;

/**
 * 归还信息Service接口
 * 
 * @author CuiGuoHao
 * @date 2023-07-29
 */
public interface IBookReturnBooksService 
{
    /**
     * 查询归还信息
     * 
     * @param returnBookId 归还信息主键
     * @return 归还信息
     */
    public BookReturnBooks selectBookReturnBooksByReturnBookId(Long returnBookId);

    /**
     * 查询归还信息列表
     * 
     * @param bookReturnBooks 归还信息
     * @return 归还信息集合
     */
    public List<BookReturnBooks> selectBookReturnBooksList(BookReturnBooks bookReturnBooks);

    /**
     * 新增归还信息
     * 
     * @param bookReturnBooks 归还信息
     * @return 结果
     */
    public int insertBookReturnBooks(BookReturnBooks bookReturnBooks);

    /**
     * 修改归还信息
     * 
     * @param bookReturnBooks 归还信息
     * @return 结果
     */
    public int updateBookReturnBooks(BookReturnBooks bookReturnBooks);

    /**
     * 批量删除归还信息
     * 
     * @param returnBookIds 需要删除的归还信息主键集合
     * @return 结果
     */
    public int deleteBookReturnBooksByReturnBookIds(Long[] returnBookIds);

    /**
     * 删除归还信息信息
     * 
     * @param returnBookId 归还信息主键
     * @return 结果
     */
    public int deleteBookReturnBooksByReturnBookId(Long returnBookId);

    void updateStatusByEndTime();
}
