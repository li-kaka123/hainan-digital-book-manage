package com.ruoyi.leaseManger.service.impl;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.ruoyi.bookManger.domain.BookBooks;
import com.ruoyi.bookManger.mapper.BookBooksMapper;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.leaseManger.domain.BookLendBooks;
import com.ruoyi.leaseManger.mapper.BookLendBooksMapper;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.leaseManger.mapper.BookReturnBooksMapper;
import com.ruoyi.leaseManger.domain.BookReturnBooks;
import com.ruoyi.leaseManger.service.IBookReturnBooksService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 归还信息Service业务层处理
 * 
 * @author CuiGuoHao
 * @date 2023-07-29
 */
@Service
public class BookReturnBooksServiceImpl implements IBookReturnBooksService 
{
    @Autowired
    private BookReturnBooksMapper bookReturnBooksMapper;

    @Autowired
    private BookLendBooksMapper bookLendBooksMapper;

    @Autowired
    private BookBooksMapper bookBooksMapper;

    @Autowired
    private ISysUserService userService;
    /**
     * 查询归还信息
     * 
     * @param returnBookId 归还信息主键
     * @return 归还信息
     */
    @Override
    public BookReturnBooks selectBookReturnBooksByReturnBookId(Long returnBookId)
    {
        return bookReturnBooksMapper.selectBookReturnBooksByReturnBookId(returnBookId);
    }

    /**
     * 查询归还信息列表
     * 
     * @param bookReturnBooks 归还信息
     * @return 归还信息
     */
    @Override
    public List<BookReturnBooks> selectBookReturnBooksList(BookReturnBooks bookReturnBooks)
    {
        return bookReturnBooksMapper.selectBookReturnBooksList(bookReturnBooks);
    }

    /**
     * 新增归还信息
     * 
     * @param bookReturnBooks 归还信息
     * @return 结果
     */
    @Override
    @Transactional
    public int insertBookReturnBooks(BookReturnBooks bookReturnBooks)
    {
        long userId = Long.parseLong(bookReturnBooks.getUserId());
        long bookId = Long.parseLong(bookReturnBooks.getBookId());
        int bookReturnNum = Integer.parseInt(bookReturnBooks.getReturnBookNum());

        //1 先判断用户是否存在
        SysUser sysUser = userService.selectUserById(userId);
        if (sysUser == null || (sysUser.getDelFlag().compareTo("0") != 0)){
            return 0;
        }

        //2 如果不存在该书籍，则图书馆不进行还
        BookBooks bookBooks = bookBooksMapper.selectBookBooksByBookId(bookId);
        if (bookBooks == null){
            return 0;
        }

        //3 根据userId和bookId先检查是否逾期，如果存在逾期的情况，插入状态显示逾期

        BookLendBooks bookLendBooks = new BookLendBooks();
        bookLendBooks.setUserId(String.valueOf(userId));
        bookLendBooks.setBookId(String.valueOf(bookId));
        List<BookLendBooks> bookLendBooks1 = bookLendBooksMapper.selectBookLendBooksList(bookLendBooks);
        //取出未归还状态的书籍
        int countNum = 0;
        List<BookLendBooks> bookLendBooks2 = new LinkedList<>();
        for (BookLendBooks lendBooks : bookLendBooks1) {
            if (Integer.parseInt(lendBooks.getLendTimeStatus()) < 3){
                bookLendBooks2.add(lendBooks);
                countNum += Integer.parseInt(lendBooks.getLendBookNum());
            }
        }
        //如果未归还总数等于需要归还的图书数目,则需要归还
        Date endTime = new Date();
        if (countNum == bookReturnNum){
            //还书数据库根据借书数据库判断是否延期
            for (BookLendBooks lendBooks : bookLendBooks2) {
                int status = Integer.parseInt(lendBooks.getLendTimeStatus());
                switch (status){
                    case 0 :
                    case 1 : bookReturnBooks.setReturnTimeStatus("0");break;
                    case 2 : bookReturnBooks.setReturnTimeStatus("1");break;
                }

                bookReturnBooks.setCreateTime(DateUtils.getNowDate());
                bookReturnBooks.setReturnTime(DateUtils.getNowDate());
                bookReturnBooks.setEndTime(lendBooks.getEndTime());
                bookReturnBooks.setReturnBookNum(lendBooks.getLendBookNum());
                bookReturnBooksMapper.insertBookReturnBooks(bookReturnBooks);

                //借书数据库归还
                bookLendBooks.setLendTimeStatus("3");
                int i = bookLendBooksMapper.updateBookLendBooks(bookLendBooks);
            }



            //总数据库归还
            int num = Integer.parseInt(bookBooks.getBookNum()) + countNum;
            bookBooks.setBookNum(String.valueOf(num));
            bookBooksMapper.updateBookBooks(bookBooks);
            return 1;
        }

        //请携带所有该类书籍一并归还/或者就是还的书籍大于借的书籍
        return 0;
    }

    /**
     * 修改归还信息
     * 
     * @param bookReturnBooks 归还信息
     * @return 结果
     */
    @Override
    public int updateBookReturnBooks(BookReturnBooks bookReturnBooks)
    {
        bookReturnBooks.setUpdateTime(DateUtils.getNowDate());
        return bookReturnBooksMapper.updateBookReturnBooks(bookReturnBooks);
    }

    /**
     * 批量删除归还信息
     * 
     * @param returnBookIds 需要删除的归还信息主键
     * @return 结果
     */
    @Override
    public int deleteBookReturnBooksByReturnBookIds(Long[] returnBookIds)
    {
        return bookReturnBooksMapper.deleteBookReturnBooksByReturnBookIds(returnBookIds);
    }

    /**
     * 删除归还信息信息
     * 
     * @param returnBookId 归还信息主键
     * @return 结果
     */
    @Override
    public int deleteBookReturnBooksByReturnBookId(Long returnBookId)
    {
        return bookReturnBooksMapper.deleteBookReturnBooksByReturnBookId(returnBookId);
    }

    @Override
    public void updateStatusByEndTime() {
        bookReturnBooksMapper.updateStatusByEndTime();
    }
}
