package com.ruoyi.leaseManger.mapper;

import java.util.List;
import com.ruoyi.leaseManger.domain.BookLendBooks;

/**
 * 借出信息Mapper接口
 * 
 * @author CuiGuoHao
 * @date 2023-07-29
 */
public interface BookLendBooksMapper 
{
    /**
     * 查询借出信息
     * 
     * @param lendBookId 借出信息主键
     * @return 借出信息
     */
    public BookLendBooks selectBookLendBooksByLendBookId(Long lendBookId);

    /**
     * 查询借出信息列表
     * 
     * @param bookLendBooks 借出信息
     * @return 借出信息集合
     */
    public List<BookLendBooks> selectBookLendBooksList(BookLendBooks bookLendBooks);

    /**
     * 新增借出信息
     * 
     * @param bookLendBooks 借出信息
     * @return 结果
     */
    public int insertBookLendBooks(BookLendBooks bookLendBooks);

    /**
     * 修改借出信息
     * 
     * @param bookLendBooks 借出信息
     * @return 结果
     */
    public int updateBookLendBooks(BookLendBooks bookLendBooks);

    /**
     * 删除借出信息
     * 
     * @param lendBookId 借出信息主键
     * @return 结果
     */
    public int deleteBookLendBooksByLendBookId(Long lendBookId);

    /**
     * 批量删除借出信息
     * 
     * @param lendBookIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBookLendBooksByLendBookIds(Long[] lendBookIds);

    public void updateStatusByEndTime();
}
